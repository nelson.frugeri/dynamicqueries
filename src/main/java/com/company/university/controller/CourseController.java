package com.company.university.controller;

import com.company.university.business.CourseBusiness;
import com.company.university.component.pagerequest.PageRequestComponent;
import com.company.university.mapper.CourseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping(value = "/course")
public class CourseController {

    @Autowired
    private CourseBusiness courseBusiness;

    @Autowired
    private PageRequestComponent pageRequestComponent;

    @GetMapping
    public ResponseEntity<?> list(
            @RequestParam(value = "id", required = false) Long idIn,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "student.id_in", required = false) List<Long> studentIdIn,
            @RequestParam(value = "student.name", required = false) String studentName,
            @RequestParam(value = "monthlypayment", required = false) BigDecimal monthlyPayment,
            @RequestParam(value = "monthlypayment_greaterthan", required = false) BigDecimal monthlyPaymentGreaterThan,
            @RequestParam(value = "monthlypayment_lessthan", required = false) BigDecimal monthlyPaymentLessThan,
            @RequestParam(value = "student.createdat_greaterthan", required = false) String createdAtGreaterThan,
            @RequestParam(value = "student.createdat_lessthan", required = false) String createdAtLessThan,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "order_by", required = false) String orderBy,
            @RequestParam(value = "sort", required = false) String sort) {

        return ResponseEntity.status(HttpStatus.OK)
                .body(CourseMapper
                        .serialize(courseBusiness
                                .list(CourseMapper.deserialize(idIn, name, studentIdIn, studentName, monthlyPayment,
                                        monthlyPaymentGreaterThan, monthlyPaymentLessThan, createdAtGreaterThan,
                                        createdAtLessThan), pageRequestComponent
                                        .pagination(offset, limit, orderBy, sort))));
    }
}
