package com.company.university.component.pagerequest;

import lombok.NoArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public class PageRequestComponent {

    public PageRequest pagination(final Integer offset,
                                  final Integer limit,
                                  final String orderBy,
                                  final String sort) {
        return PageRequest.of(offset != null ? offset : 0,
                limit != null ? limit : 20,
                orderBy != null ? sort != null
                        && sort.equalsIgnoreCase("ASC")
                        ? Sort.by(Sort.Direction.ASC, orderBy)
                        : Sort.by(Sort.Direction.DESC, orderBy)
                        : Sort.by(Sort.Direction.DESC, "id"));
    }
}
