package com.company.university.component.dynamicquery;

import org.springframework.data.jpa.domain.Specification;

public interface QueryEnum<E> {
    Specification<E> clause(Object value);
}
