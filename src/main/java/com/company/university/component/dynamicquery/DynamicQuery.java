package com.company.university.component.dynamicquery;

import org.springframework.data.jpa.domain.Specification;

public interface DynamicQuery<T extends Enum<T>, E> {

    T[] values();

    Specification<E> perform(T query, Object value);
}
