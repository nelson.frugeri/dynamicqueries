package com.company.university.component.dynamicquery;

import lombok.NonNull;
import org.springframework.data.jpa.domain.Specification;

import java.lang.reflect.Field;

public class GenerationDynamicQuery {

    public static <T> Specification<T> run(@NonNull final Object queryParam,
                                           @NonNull final DynamicQuery dynamicQuery) {

        Specification<T> query = Specification.where(null);
        Class<?> clazz = queryParam.getClass();

        for (Field field : queryParam.getClass().getDeclaredFields()) {
            try {
                final var queryParamValue = clazz.getField(field.getName()).get(queryParam);

                for (Enum value : dynamicQuery.values()) {
                    if (field.getName().toUpperCase().equals(value.name().toUpperCase())
                            && queryParamValue != null) {
                        query = query.and(dynamicQuery.perform(value, queryParamValue));
                        break;
                    }
                }
            } catch (NoSuchFieldException | IllegalAccessException ignored) {
            }
        }

        return query;
    }
}
