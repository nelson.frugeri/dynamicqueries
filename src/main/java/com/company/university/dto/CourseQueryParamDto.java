package com.company.university.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CourseQueryParamDto implements Serializable {

    private static final long serialVersionUID = -9071620029447346246L;

    public Long idIn;
    public String name;
    public List<Long> studentIdIn;
    public String studentName;
    public BigDecimal monthlyPayment;
    public BigDecimal monthlyPaymentGreaterThan;
    public BigDecimal monthlyPaymentLessThan;
    public CreatedAtGreaterAndLessThan studentCreatedAt;

    @Builder
    @Getter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CreatedAtGreaterAndLessThan implements Serializable {

        private static final long serialVersionUID = 3762882466170615617L;

        public ZonedDateTime createdAtGreaterThan;
        public ZonedDateTime createdAtLessThan;
    }
}
