package com.company.university.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseCourseDto implements Serializable {

    private static final long serialVersionUID = 1114831875635065317L;

    private Long id;
    private String name;
    private String description;
    private List<StudentDto> students;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class StudentDto implements Serializable {

        private static final long serialVersionUID = 4553986498580868563L;

        private Long id;
        private String name;
        private BigDecimal monthyPayment;
        private ZonedDateTime createdAt;
        private ZonedDateTime updatedAt;
    }
}
