package com.company.university.business;

import com.company.university.component.dynamicquery.GenerationDynamicQuery;
import com.company.university.domain.Course;
import com.company.university.dto.CourseQueryParamDto;
import com.company.university.dynamicquery.CourseDynamicQuery;
import com.company.university.repository.CourseRepository;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class CourseBusiness {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private CourseDynamicQuery courseDynamicQuery;

    public Page<Course> list(@NonNull final CourseQueryParamDto courseQueryParamDto,
                             @NonNull final PageRequest pageRequest) {
        return courseRepository.findAll(GenerationDynamicQuery
                .run(courseQueryParamDto, courseDynamicQuery), pageRequest);
    }
}
