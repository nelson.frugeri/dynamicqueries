package com.company.university.dynamicquery;

import com.company.university.component.dynamicquery.DynamicQuery;
import com.company.university.domain.Course;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public class CourseDynamicQuery implements DynamicQuery<CourseDao, Course> {

    @Override
    public CourseDao[] values() {
        return CourseDao.values();
    }

    @Override
    public Specification<Course> perform(CourseDao query, Object value) {
        return query.clause(value);
    }
}
