package com.company.university.dynamicquery;

import com.company.university.component.dynamicquery.QueryEnum;
import com.company.university.domain.Course;
import com.company.university.dto.CourseQueryParamDto;
import lombok.NonNull;
import org.springframework.data.jpa.domain.Specification;

import java.math.BigDecimal;
import java.util.List;

import static java.text.MessageFormat.format;

public enum CourseDao implements QueryEnum<Course> {
    IDIN {
        @Override
        public Specification<Course> clause(@NonNull final Object value) {
            return (course, criteriaQuery, criteriaBuilder) -> criteriaBuilder
                    .equal(course.get("id"), (Long) value);
        }
    },
    NAME {
        @Override
        public Specification<Course> clause(@NonNull final Object value) {
            return (course, criteriaQuery, criteriaBuilder) -> criteriaBuilder
                    .like(criteriaBuilder.upper(course.get("name")),
                            (String) format("%{0}%", value.toString().toUpperCase()));
        }
    },
    STUDENTIDIN {
        @Override
        public Specification<Course> clause(@NonNull final Object value) {
            return (course, criteriaQuery, criteriaBuilder) -> course.join("students")
                    .get("id").in((List) value);
        }
    },
    STUDENTNAME {
        @Override
        public Specification<Course> clause(@NonNull final Object value) {
            return (course, criteriaQuery, criteriaBuilder) -> criteriaBuilder
                    .like(criteriaBuilder.upper(course.join("students").get("name")),
                            (String) format("%{0}%", value.toString().toUpperCase()));
        }
    },
    MONTHLYPAYMENT {
        @Override
        public Specification<Course> clause(@NonNull final Object value) {
            return (course, criteriaQuery, criteriaBuilder) -> criteriaBuilder
                    .equal(course.join("students").get("monthlyPayment"), (BigDecimal) value);
        }
    },
    MONTHLYPAYMENTGREATERTHAN {
        @Override
        public Specification<Course> clause(@NonNull final Object value) {
            return (course, criteriaQuery, criteriaBuilder) -> criteriaBuilder
                    .greaterThan(course.join("students").get("monthlyPayment"), (BigDecimal) value);
        }
    },
    MONTHLYPAYMENTLESSTHAN {
        @Override
        public Specification<Course> clause(@NonNull final Object value) {
            return (course, criteriaQuery, criteriaBuilder) -> criteriaBuilder
                    .lessThan(course.join("students").get("monthlyPayment"), (BigDecimal) value);
        }
    },
    STUDENTCREATEDAT {
        @Override
        public Specification<Course> clause(@NonNull final Object value) {
            CourseQueryParamDto.CreatedAtGreaterAndLessThan createdAt =
                    (CourseQueryParamDto.CreatedAtGreaterAndLessThan) value;

            return (course, criteriaQuery, criteriaBuilder) -> criteriaBuilder
                    .between(course.join("students").get("createdAt"),
                            createdAt.getCreatedAtGreaterThan(),
                            createdAt.getCreatedAtLessThan());
        }
    };
}
