package com.company.university.mapper;

import com.company.university.domain.Course;
import com.company.university.dto.CourseQueryParamDto;
import com.company.university.dto.ResponseCourseDto;
import lombok.NonNull;
import org.springframework.data.domain.Page;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static java.text.MessageFormat.format;

public class CourseMapper {

    public static List<ResponseCourseDto> serialize(@NonNull final Page<Course> courses) {
        List<ResponseCourseDto> response = new ArrayList<>();

        courses.getContent().forEach(course -> {
            List<ResponseCourseDto.StudentDto> students = new ArrayList<>();

            course.getStudents().forEach(student -> {
                students.add(ResponseCourseDto.StudentDto.builder()
                        .id(student.getId())
                        .name(student.getName())
                        .monthyPayment(student.getMonthlyPayment())
                        .build());
            });

            response.add(ResponseCourseDto.builder()
                    .id(course.getId())
                    .name(course.getName())
                    .description(course.getDescription())
                    .students(students)
                    .build());
        });

        return response;
    }

    public static CourseQueryParamDto deserialize(final Long idIn, final String name,
                                                  final List<Long> studentIdIn, final String StudentName,
                                                  final BigDecimal monthlyPayment,
                                                  final BigDecimal monthlyPaymentGreaterThan,
                                                  final BigDecimal monthlyPaymentLessThan,
                                                  final String createdAtGreaterThan,
                                                  final String createdAtLessThan) {

        return CourseQueryParamDto.builder()
                .idIn(idIn)
                .name(name)
                .studentIdIn(studentIdIn)
                .studentName(StudentName)
                .monthlyPayment(monthlyPayment)
                .monthlyPaymentGreaterThan(monthlyPaymentGreaterThan)
                .monthlyPaymentLessThan(monthlyPaymentLessThan)
                .studentCreatedAt(getCreatedAt(createdAtGreaterThan, createdAtLessThan))
                .build();
    }

    private static CourseQueryParamDto.CreatedAtGreaterAndLessThan getCreatedAt(final String createdAt,
                                                                                final String createdAtLessThan) {

        return createdAt != null ? CourseMapper.setCreatedAt(createdAt, createdAtLessThan) : null;
    }

    private static CourseQueryParamDto.CreatedAtGreaterAndLessThan setCreatedAt(@NonNull final String createdAt,
                                                                                final String createdAtLessThan) {

        CourseQueryParamDto.CreatedAtGreaterAndLessThan.CreatedAtGreaterAndLessThanBuilder createdAtGreaterAndLessThan =
                CourseQueryParamDto.CreatedAtGreaterAndLessThan.builder();

        createdAtGreaterAndLessThan.createdAtGreaterThan(ZonedDateTime
                .parse(format("{0} 00:00:00 {1}", createdAt, ZoneId.systemDefault()),
                        DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z")));

        createdAtGreaterAndLessThan.createdAtLessThan(createdAtLessThan != null
                ? ZonedDateTime.parse(format("{0} 23:59:59 {1}", createdAtLessThan,
                ZoneId.systemDefault()), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z"))
                : ZonedDateTime.parse(format("{0} 23:59:59 {1}", createdAt,
                ZoneId.systemDefault()), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss z")));

        return createdAtGreaterAndLessThan.build();
    }
}
